<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">

<head>

<title></title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>

<body>

<form method = "post">

    <div class="form-group">
        <fieldset>
         <legend> <h1> Book Title: </h1></legend>

         <div class="form-group"> <label> Book ID :</label> <br>
            <input type="text" name="Book ID" class="form-control" placeholder="Enter ID"><br>
             </div>

         <div class="form-group"> <label> Book Name: </label> <br>
            <input type="text" name="Book Name" class="form-control" placeholder="Enter the name"><br>
             </div>

            <div class="form-group"> <label> Author's Name: </label> <br>
                <input type="text" name="Author's Name" class="form-control" placeholder="Enter author's name"><br>
             </div><br>

            <div>
                <button type="button" class="btn btn-primary col-xs-12 col-sm-6 col-md-2">Back</button>
                <button type="button" class="btn btn-primary col-xs-12 col-sm-6 col-md-2">Save & Add</button>
                <button type="button" class="btn btn-primary col-xs-12 col-sm-6 col-md-2">View List</button>
                <button type="button" class="btn btn-primary col-xs-12 col-sm-6 col-md-2">Reset</button>
            </div>
         </fieldset>
    </div>
</form>

</body>
</html>